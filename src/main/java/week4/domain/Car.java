package week4.domain;

public class Car implements  Vehicle{
    public void start() {
        System.out.println("Moves like car");
    }
    public void stop() {
    }

    @Override
    public void move(int kilometers) {
        System.out.println("Masina s-a deplasat "+kilometers+" km");

    }
   public void shiftGear(int i) {
      System.out.println("Suntem in viteza "+ i+ "cu Car");
  };
  //  public void drive(float km){}
  public void drive(double km) {}

    public double getAvailableFuel() {
        return 0;
    }
}
