package week4.domain;

public interface Vehicle {
    void start () ;
    void stop();
    void move(int kilometers);

  //  void shiftGear(int i);
}
