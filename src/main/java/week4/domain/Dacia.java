package week4.domain;

public class Dacia extends Car{
    public void start() {
        System.out.println("Moves like Dacia");
    }
    public void stop() {
    }

    @Override
    public void move(int kilometers) {
        System.out.println("Dacia s-a deplasat "+kilometers+" km");

    }
    public void shiftGear(int i) {
        System.out.println("Suntem in viteza "+ i+ "cu Dacia");
    }

 //   public void drive(float km) {}
    public void drive(double km) {}
}

