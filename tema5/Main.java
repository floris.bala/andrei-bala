package week5.domain;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.*;
import java.io.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Person person1= new Hired ("AGhita",24);
        Person person2= new Student ("BMishu",20);
        Person person3= new Unemployed("CMishulica",21);
        Person person4=new Student("Zoli",32);
        /*List<Person> persoane=new ArrayList<>();
        persoane.add(person1);
        persoane.add(person2);
        System.out.println(persoane);
        Set<Person> perset =new HashSet<>();
        perset.add(person1);
        perset.add(person2);
        perset.add(person3);
        System.out.println(perset);
        person2.equals(person3);*/
        ArrayList<Person> al=new ArrayList<Person>();
        al.add(person1);
        al.add(person2);
        al.add(person3);
        al.add(person4);
        Set<Person> person = new TreeSet<>();
        person.add(person1);
        person.add(person2);
        person.add(person3);
        person.add(person4);
        System.out.println("Sortare dupa varsta: " +person);
        System.out.println("Sortare dupa nume:");
        Collections.sort(al,new NameSort());
        for(Person st: person){
            System.out.println(st.name+" "+st.age);
        }
        HashMap<Person,List<Hobby>> hm = new HashMap<>();

        ArrayList<String> address= new ArrayList<>();
        // Add elements in the array list
        address.add("Rimetea");
        address.add("Liteni");
        address.add("Postavaru");
        // convert the arraylist into a string
        String arraylist = String.join(", ", address);
       // System.out.println("String: " + arraylist);
        Person pers1= new Hobby ("Ahita",2,"parapanta",1, arraylist);
    //    Person pers2= new Hobby ("BMishu",20,"ciclism",2,"Cluj-Budapesta");
        System.out.println(pers1);
    }
}

